# Required Third-party Libraries
* Tornado
* OpenCV 2
* PIL

# To Execute
0. Make sure the libraries above have been successfully installed
1. indexing images with api: all of the options are required.
   First set the Data_Dir in inventory.py to the folder stores the image data
   Then `cd` into `src` folder and do `python indexer.py --mode=[{"RGB" or "HSV" or "HSV_Middle"}]`

2. starting server by: please make sure to indexing the corresponding
     descriptor first before doing this!
   `python start.py --mode=[{"RGB" or "HSV" or "HSV_Middle"}]`

3. Go to the front-end page in browser and upload image

4. See result in snippets

5. Click for original image

# Acknowledgement
The code about servers setup, class modularity is largely derived from the
assignment code provided by Mr. Matt Doherty in his Search Engine Architecture
class at NYU for Spring 2015. The team also refers to code on Adrian Rosebrock's
blog.
