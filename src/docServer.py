import tornado
from tornado import web, gen, process, httpserver, httpclient, netutil
import numpy as np
from PIL import Image
import StringIO
import pickle
from sets import Set
import inventory as iv
import os
import sys


SETTINGS = {"static_path": "../resources/"}

snippetWidth = 100


class Doc():
  snippetCache = Set()

  class snippetHandler(web.RequestHandler):
    def initialize(self, data):
      # data is a tuple: [path_to_picture_folder, mappings_from_id_to_file_name]
      # so start.py should set up these and initialize index server accordingly
      self._path, self._img_mapping = data
      self._snippetpath = "%s/%s" % (self._path, "snippet/")

    def get(self):
      img =  self.get_argument('id', None)
      mode = self.get_argument('mode', None)
      if img is None:
        return
      img = int(img)
      path = "%s/%s" % (self._path, self._img_mapping[img])

      if mode is None:
        # open image and prepare to transmit full image
        #h = IndexDotHTMLAwareStaticFileHandler(self._img_mapping[img],{'path':self._path})
        self.write(open(path).read())
      else:
        # Snippet
        # check if already existed
        if self._img_mapping[img] not in Doc.snippetCache:
          # open image
          pic = Image.open(path)
          # preserve ratio and create thumbnail
          w, h = pic.size
          pic.thumbnail((snippetWidth,h*snippetWidth/w))
          # save snippet
          if not os.path.exists(self._snippetpath):
            os.makedirs(self._snippetpath)
          pic.save(self._snippetpath+self._img_mapping[img])
          # record
          Doc.snippetCache.add(self._img_mapping[img])
        # read snippet
        self.write(open(self._snippetpath+self._img_mapping[img]).read())

      self.set_header("Content-type",  "image/png")
      self.finish()


      

  
# non-snipet requests use this handler
class IndexDotHTMLAwareStaticFileHandler(web.StaticFileHandler):
  def parse_url_path(self, url_path):
    return super(IndexDotHTMLAwareStaticFileHandler, self).parse_url_path(url_path)



# For test only
if __name__ == "__main__":
<<<<<<< HEAD
  img_mapping = "%s/doc%d.pkl" % (inventory.Doc_Dir, 0)
  #mappings = pickle.load(open(img_mapping,'r'))
  mappings = {}
  mappings[0] = "Lenna.png"
  data = [inventory.Data_Dir, mappings]
=======
>>>>>>> 802edc9407761d5d1c834d98750d3e80284b4863

  
  indexh = Doc()
  try:
    docFile = pickle.load(open("%s%s%s/doc%d.pkl" % (iv.PWD,
      iv.Img_subPath["RGB"], iv.Doc_Dir, 0), 'r'))
  except Exception, e:
    print >> sys.stderr, "Exception: %s" % str(e)
    sys.exit(1)

  data = [iv.Data_Dir, docFile]


  application = httpserver.HTTPServer(web.Application([
      (r'/snippets', Doc.snippetHandler, dict(data=data)),
      #(r'/img', indexHandler.imgHandler),
      (r'/(.*)', IndexDotHTMLAwareStaticFileHandler, dict(path = iv.Data_Dir)),
      ]))
  application.listen(13334)
  tornado.ioloop.IOLoop.instance().start()
