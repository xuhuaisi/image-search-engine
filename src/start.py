import logging
import tornado
from tornado.ioloop import IOLoop
from tornado import web, process, httpserver, netutil
import pickle
import signal
import inventory as iv
import indexServer
import frontend
import docServer
import argparse
import sys

SETTINGS = {"static_path": iv.PWD+"/webapp"}

def signal_handler(signum, frame):
    IOLoop.instance().stop()

class argparser():
  args = ''
  def __init__(self):
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--mode',
      choices = ['RGB', 'HSV', 'HSV_Middle'],
      required=True,
      help='RGB or HSV or HSV_Middle')
    self.args = parser.parse_args()

def main():
  arg = argparser()

  print iv.Img_subPath[arg.args.mode]
  signal.signal(signal.SIGINT, signal_handler)
  n_process = iv.N_indexSrv + iv.N_docSrv + 1
  taskID = process.fork_processes(n_process, max_restarts=0)
  port = iv.BASE_PORT + taskID

  if taskID == 0:
    # frontend
    #app = httpserver.HTTPServer(web.Application([
    #    (r"/upload", frontend.Web, dict(mode=arg.args.mode)),
    #    (r"/(.*)", frontend.IndexDotHTMLAwareStaticFileHandler, dict(path = SETTINGS['static_path']))]))
    app = httpserver.HTTPServer(tornado.web.Application([
      (r"/upload", frontend.Web, dict(mode=arg.args.mode)),
      (r"/(.*)", frontend.IndexDotHTMLAwareStaticFileHandler, dict(path=SETTINGS['static_path']))
      ], **SETTINGS))
    logging.info("Front end is listening on " + str(port))
    
  else:
    if taskID <= iv.N_indexSrv:
      # Index server
      serverID = taskID - 1
      # read corresponding index file
      try:
        indexFile = pickle.load(open("%s%s%s/index%d.pkl" % (iv.PWD, 
          iv.Img_subPath[arg.args.mode], iv.Idx_Dir, serverID), 'r'))
      except Exception, e:
        print >> sys.stderr, "Exception: %s" % str(e)
        sys.exit(1)

      app = httpserver.HTTPServer(web.Application([(r"/index", indexServer.Index, dict(data=indexFile))]))
      logging.info("Index server %d listening on %d" % (serverID, port))
    else:
      # doc server
      serverID = taskID - iv.N_indexSrv - 1
      # read corresponding doc file

      try:
        docFile = pickle.load(open("%s%s%s/doc%d.pkl" % (iv.PWD,
          iv.Img_subPath[arg.args.mode], iv.Doc_Dir, serverID), 'r'))
      except Exception, e:
        print >> sys.stderr, "Exception: %s" % str(e)
        sys.exit(1)

      data = [iv.Data_Dir, docFile]
      ndexh = docServer.Doc()
      app = httpserver.HTTPServer(web.Application([(r"/img", ndexh.snippetHandler, dict(data=data))]))
      logging.info("Image server %d listening on %d" % (serverID, port))

  app.add_sockets(netutil.bind_sockets(port))
  IOLoop.current().start()

if __name__ == "__main__":
  logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.DEBUG)
  main()
