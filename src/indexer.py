import os
import pickle
import inventory
import argparse
from imageConverter import ImageConverter
from collections import defaultdict
import inventory as iv


class argparser():
  args = ''
  def __init__(self):
    parser = argparse.ArgumentParser(description='Demo')
    parser.add_argument('--mode',
      choices = ['RGB', 'HSV', 'HSV_Middle'],
      required=True,
      help='RGB or HSV or HSV_Middle')
    self.args = parser.parse_args()

def main():
  arg = argparser()
  print arg.args.mode

  index = [[] for i in range(iv.N_indexSrv)]
  doc = [defaultdict(int) for i in range(iv.N_docSrv)]
  converter = ImageConverter()

  img_id = 0
  for root, dirs, files in os.walk(iv.Data_Dir):
    for file in files:
      # get corresponding feature vector based on command line argument
      vector = converter.convert("%s/%s" % (iv.Data_Dir, file), \
          None, arg.args.mode)
      if vector is None:
        continue
      index[img_id%iv.N_indexSrv].append({'histo':vector, 'img_id':img_id})
      doc[img_id%iv.N_docSrv][img_id] = file
      img_id += 1
      if (img_id+1)%20 == 0:
        print "%d images have been processed" % (img_id+1)
    # only search immediate folder
    break
  
  # check if dir exists
  Idx_folder = "%s%s%s" % (iv.PWD, iv.Img_subPath[arg.args.mode], iv.Idx_Dir)
  Doc_folder = "%s%s%s" % (iv.PWD, iv.Img_subPath[arg.args.mode], iv.Doc_Dir)

  if not os.path.exists(Idx_folder):
    os.makedirs(Idx_folder)
  if not os.path.exists(Doc_folder):
    os.makedirs(Doc_folder)

  # save result
  for ix, idx in enumerate(index):
    pickle.dump(index[ix], open("%s/index%d.pkl" % (Idx_folder, ix), "w"))
  for ix, do in enumerate(doc):
    pickle.dump(doc[ix], open("%s/doc%d.pkl" % (Doc_folder, ix), "w"))




if __name__ == "__main__":
  main()
