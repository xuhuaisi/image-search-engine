import tornado
from tornado import web, gen, httpclient
import pickle
import numpy as np
import inventory as iv


def matchingResult(histA, histB, eps = 1e-10):

  # compute the chi-squared distance
  d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps)
          for (a, b) in zip(histA, histB)])
  # return the chi-squared distance
  return d




class Index(web.RequestHandler):
  def initialize(self, data):
    # data is histogram to be searched
    self._score = data
  def post(self):
    # query is a dictionary {'histo': [...], 'img_id': x}
    query = self.get_argument('data',None)
    if query is None:
      return
    q = pickle.loads(query)
    # calculate matching scores
    result = [[matchingResult(q, x['histo']), x['img_id']] for x in self._score]
    # return the first 10 pictures that matches the query picture
    self.write(pickle.dumps(sorted(result, key=lambda pair:pair[0])[: iv.N_results]))



def test():
  
  data = np.load('../test/tmp.out')
  print matchingResult(data,data)


# For test only
if __name__ == "__main__":
  test()





