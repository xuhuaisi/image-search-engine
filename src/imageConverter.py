# cite pyimage
import numpy as np
import cv2
import os

debug = 0

class ImageConverter():
    # for various bins
    #def __init__(self, bins = [8, 8, 8]):
    #    self.bins = bins

    def descriptorRGB(self, image):
        if debug:
            cv2.imshow("Result", image)
            cv2.waitKey(0)

        # transform an image into a vector
        hist = cv2.calcHist([image], [0, 1, 2],\
                None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
        hist = cv2.normalize(hist).flatten()
        return hist


    def descriptorHSV(self, image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        if debug:
            cv2.imshow("Result", image)
            cv2.waitKey(0)

        hist = cv2.calcHist([image], [0, 1, 2], None, [8, 12, 3],
		[0, 180, 0, 256, 0, 256])
	hist = cv2.normalize(hist).flatten()
        return hist


    def descriptorHSV_middle(self, image):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        # construct a mask to filter out the middle of the image
        (h, w) = image.shape[:2]
        (cX, cY) = (int(w * 0.5), int(h * 0.5))
        (axesX, axesY) = (int(w * 0.75) / 2, int(h * 0.75) / 2)
        ellipMask = np.zeros(image.shape[:2], dtype = "uint8")
	cv2.ellipse(ellipMask, (cX, cY), (axesX, axesY), 0, 0, 360, 255, -1)

        if debug == 1:
            res = cv2.bitwise_and(image,image,mask = ellipMask)
            cv2.imshow("Result", res)
            cv2.waitKey(0)


        hist = cv2.calcHist([image], [0, 1, 2], ellipMask, [8, 12, 3],
		[0, 180, 0, 256, 0, 256])
	hist = cv2.normalize(hist).flatten()
        return hist



    def convert(self, imagePath, writePath = None, mode = 'RGB'):
        # compute a 3D histogram in the RGB colorspace,
        # then normalize the histogram so that images
        # with the same content, but either scaled larger
        image = cv2.imread(imagePath)
        # check if image if valid and correctly read
        if image is None:
            return None
            
        if mode == 'RGB':
            hist = self.descriptorRGB(image)
        elif mode == 'HSV':
            hist = self.descriptorHSV(image)
        elif mode == 'HSV_Middle':
            hist = self.descriptorHSV_middle(image)

        if writePath is not None:
            try:
                with open(writePath, 'w') as f:
                    np.save(f, hist)
            except:
                print 'Error on writing histogram to disk.'

        return hist

def test():
    global debug
    ic = ImageConverter()
    debug = 1
    res = ic.convert('../resources/flower.jpg', '../test/tmp.out','RGB')
    print res

if __name__ == '__main__':
    test()
