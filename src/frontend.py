import tornado
from tornado.ioloop import IOLoop
from tornado import gen, web, httpclient, template, httpserver, netutil
from PIL import Image
import StringIO
import pickle
import urllib
import logging

import inventory as iv
import imageConverter

# Don't forget to cite Matt's code
SETTINGS = {"static_path": iv.PWD+"/webapp"}
RESOURCES_PATH = '../resources/'

prev = None

class IndexDotHTMLAwareStaticFileHandler(web.StaticFileHandler):
    def parse_url_path(self, urlPath):
        if not urlPath or urlPath.endswith('/'):
            urlPath += 'index.html'
        return super(IndexDotHTMLAwareStaticFileHandler, self).parse_url_path(urlPath)

class Web(web.RequestHandler):
    def initialize(self, mode):
      self._mode = mode
    @gen.coroutine
    def post(self):
        fileInfo = self.request.files['image'][0]

        name = fileInfo['filename']
        suffix = name.split('.')[-1]

        # write image to the disk
        try:
          img = Image.open(StringIO.StringIO(fileInfo['body']))
          imgPath = RESOURCES_PATH + 'uploadedImage.' + suffix
          img.save(imgPath, img.format)

          hist = imageConverter.ImageConverter().convert(imgPath, None, self._mode)
        except:
          # HACK!!
          # resolve the back-button issue on Chrome
          global prev
          hist = prev
        prev = hist
        # obtain IDs of similar images from each index server
        responses = yield self.getIndexResults(pickle.dumps(hist))
        results = []

        # aggreate results and pick the top 10
        for response in responses:
          results += pickle.loads(response.body)
        results = sorted(results, key = lambda pair: pair[0])[: iv.N_results]
        
        print results
        '''
        for pair in results:
            imgId = pair[1]
            serverId = imgId % 3
            htmlStrings.append('<img src="' + iv.servers['doc'][serverId] +\
                '/img?id=' + str(imgId) + '">')
        t = ''
        for s in htmlStrings:
            t += s

        print t
        '''
        resultsMap = []
        for pair in results:
            tmp = []
            tmp.append(pair[0])
            tmp.append(pair[1])
            tmp.append(tmp[1] % 3)
            tmp.append(iv.servers['doc'][tmp[2]])
            resultsMap.append(tmp)

        loader = template.Loader('webapp')
        temp = loader.load('upload.html')

        self.write(temp.generate(searchResults = resultsMap))

        # self.render('webapp/upload.html')


    @gen.coroutine
    def getIndexResults(self, data):
        # fetch most similar images
        http = httpclient.AsyncHTTPClient()
        body = urllib.urlencode({'data': data})
        responses = yield [http.fetch('http://%s/index' % server, None,
            method = 'POST', headers = None,  body = body)
            for server in iv.servers['index']]

        '''
        body = urllib.urlencode({'data':data})
        responses = yield [http.fetch('http://%s/index' %
            (server), None, method = "POST", headers=None, body=body)
            for server in iv.servers['index']]

        '''
        raise gen.Return(responses)

if __name__ == '__main__':

    application = httpserver.HTTPServer(tornado.web.Application([
      (r'/upload', Web),
      (r"/(.*)", IndexDotHTMLAwareStaticFileHandler, dict(path=SETTINGS['static_path']))
      ], **SETTINGS))
    logging.info("Front end is listening on " + str(8888))
    application.add_sockets(netutil.bind_sockets(8888))
    IOLoop.current().start()

