# https://www.pyimagesearch.com/2014/01/22/clever-girl-a-guide-to-utilizing-color-histograms-for-computer-vision-and-image-search-engines/

from matplotlib import pyplot as plt
import numpy as np
import argparse
import cv2

image = cv2.imread('../resources/Lenna.png')

cv2.imshow('Image', image)

chans = cv2.split(image)
colors = ('b', 'g', 'r')

plt.figure()
plt.title('Image Histogram')
plt.xlabel('Bins')
plt.ylabel('# of Pixels')

features = []

for (chan, color) in zip(chans, colors):
    hist = cv2.calcHist([chan], [0], None, [256], [0, 256])
    features.extend(hist)

    plt.plot(hist, color = color)
    plt.xlim([0, 256])

plt.show()
cv2.waitKey(0)
